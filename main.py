from fastapi import FastAPI
import uvicorn 
import json
from uuid import UUID
from typing import Union
from pydantic import BaseModel



class professors(BaseModel):
    professor_id: UUID
    name: str
    year_of_entry: int
    
    
class classes(BaseModel):
    class_id: UUID
    name: str
    professor: professors

class courses(BaseModel):
    courses_id: UUID
    name: str
    workload: float
    
class students(BaseModel):
    students_id: UUID
    name: str
    CPF: str
    year_of_entry: int
    course: courses 
    class_: classes



app = FastAPI(
    title = "PROGRAD_API",
    version = "0.0.1",
    contact={
        "name": "PROGRAD UFAL",
        "email": "PROGRADexample@example.com",
    },    
)



@app.get("/students", summary='Fetch all students data', tags=["Students"])
async def all_data_students(students:students):
    return students

@app.post("/students", summary = 'Add a new student', tags=["Students"])
async def add_students():
    return "create students item"

@app.get("/students/{students_id}", summary = 'Fetch student data', tags=["Students"])
async def read_student(students_id: int):
        return students_id

@app.put("/students/{students_id}/course/{course_id}", summary = "Set or change student's course", tags=["Students"])
async def update_student(id: int):
        return "update students item with id {id}"

@app.post("/students/{students_id}/class/{class_id}", summary = 'Add student to a class', tags=["Students"])
async def creat_students():
    return "create students item"

@app.delete("/students/{students_id}/class/{class_id}", summary = 'Remove student from a class', tags=["Students"])
async def update_students(id: int):
        return "delete students item with id {id}"




@app.get("/professors", summary='Fetch all professors data', tags=["Professors"])
async def professors(professors:professors):
    return professors

@app.post("/professors", summary='Add a new professor', tags=["Professors"])
async def creat_professors():
    return "create professors item"

@app.get("/professors/{professors_id}", summary='Fetch professor data', tags=["Professors"])
async def read_professors(id: int):
        return "read professors item with id {id}"



@app.get("/courses", summary='Fetch all courses data', tags=["Courses"])
async def courses():
    sumary='Fetch all students data'
    return courses

@app.get("/courses/{courses_id}", summary='Fetch course data', tags=["Courses"])
async def read_courses(id: int):
        return "read courses item with id {id}"

@app.post("/courses", summary='Add a new course', tags=["Courses"])
async def creat_courses():
    return "create courses item"

@app.get("/courses/{courses_id}/students", summary='Fetch students of course', tags=["Courses"])
async def read_courses(id: int):
        return "read courses item with id {id}"

@app.put("/courses/{courses_id}/coordinator/{professor_id}", summary='Set or change a professor as the course coordinator', tags=["Courses"])
async def update_courses_coordinator(id: int):
        return "update students item with id {id}"



@app.get("/classes", summary='Fetch all classes data', tags=["Classes"])
async def classes():
    return courses

@app.get("/classes/{class_id}", summary='Fetch class data', tags=["Classes"])
async def read_classes(id: int):
        return "read classes item with id {id}"

@app.post("/classes", summary='Add a new class', tags=["Classes"])
async def creat_classes():
    return "create classes item"

@app.get("/classes/{class_id}/students", summary='Fetch students of class', tags=["Classes"])
async def read_classes(id: int):
        return "read classes item with id {id}"

@app.put("/classes/{class_id}/teacher/{professor_id}", summary='Set or change a professor as the class teacher', tags=["Classes"])
async def update_classes(id: int):
        return "update classes item with id {id}"

@app.get("/classes/ranking", summary='Get a ranking of classes by number of their enrolled students', tags=["Classes"])
async def read_classes(id: int):
        return "read classes item with id {id}"



if __name__ == "__main__":
    uvicorn.run(app='main:app', host="0.0.0.0", port=3000, reload=True)