from fastapi import FastAPI

app = FastAPI(
    title = "PROGRAD_API",
    description=description,
    version = "0.0.1",
    contact={
        "name": "PROGRAD UFAL",
        "email": "PROGRADexample@example.com",
    },    
)

@app.get("/", tags=["Students"])
async def read_students():
    return [{"name": "Roberaldo"}]